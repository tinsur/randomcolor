export function getRandomColor() {
    let x=randomInteger(0, 255)
    let y=randomInteger(0, 255)
    let z=randomInteger(0, 255)
    let a=Math.random()
    return `${x},${y},${z},${a}`
}

function randomInteger(min, max) {
  // универсальная функция для случайного числа от min до (max+1)
  let rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
}
